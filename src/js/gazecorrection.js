// Initialize Rendering
var videoInput = document.getElementById('video');
var width = videoInput.width;
var height = videoInput.height;
var scene = new THREE.Scene();
var camera = new THREE.OrthographicCamera( 0, width, height, 0, 50, 1000 );

var webglOutput = document.getElementById('webgl');
var renderer = new THREE.WebGLRenderer( { canvas: webglOutput, alpha: true } );
renderer.setSize(width, height);
camera.position.z = 300;

// Initialize Deformation
var deformer = new LaplacianDeformer();

// Texture
videoImage = document.getElementById( 'videoImage' );
videoImageContext = videoImage.getContext( '2d' );
// background color if no video present
videoImageContext.fillStyle = '#000000';
videoImageContext.fillRect( 0, 0, videoImage.width, videoImage.height );

videoTexture = new THREE.Texture( videoImage );
videoTexture.minFilter = THREE.LinearFilter;
videoTexture.magFilter = THREE.LinearFilter;

// Export mesh using Blender three.js exporter plugin
var materialWireframe = new THREE.MeshBasicMaterial( { color: 0x2194ce, wireframe: true } )
var materialShader = new THREE.ShaderMaterial({
  uniforms: {
    color: {type: 'f', value: 0.0},
    texture: { type: "t", value: videoTexture },
    zOffset: { type: "f", value: 0.0},
    rotationMatrix : { type: "m4", value: new THREE.Matrix4() },
  },
  vertexShader: document.getElementById('vertexShader').text,
  fragmentShader: document.getElementById('fragmentShader').text
});


// Create background
var backgroundGeometry = new THREE.PlaneBufferGeometry( 640, 480, 1, 1 );
var textureShader = new THREE.MeshBasicMaterial({map: videoTexture});
var plane = new THREE.Mesh( backgroundGeometry, textureShader );
plane.geometry.applyMatrix(new THREE.Matrix4().makeTranslation(320, 240, 0));
scene.add( plane );

var faceMeshGroup = new THREE.Object3D();

var faceMesh = null;
var originalMesh = null;
var loader = new THREE.JSONLoader();
//loader.load('obj/generic_mesh.json',
loader.load('obj/generic_mesh_tiny2.json',
  function(geometry, materials) {

    faceMesh = new THREE.Mesh(geometry, materialShader);

    faceMesh.geometry.dynamic = true;

    originalMesh = faceMesh.clone();
    //scene.add(faceMesh);

    deformer.initialize(originalMesh, matching);

    faceMeshGroup.add(faceMesh);
  }
);

var geometry = new THREE.SphereGeometry( 5, 8, 8 );
var material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
var sphere = new THREE.Mesh(geometry, material);
sphere.visible = false;

var matchedPoints = [];
for (var i = 0; i < matching.length; i++) {
  var newSphere = sphere.clone();
  matchedPoints.push(newSphere);
  scene.add(newSphere);
}

// Create Eyeballs
var eyeGeometry = new THREE.SphereGeometry( 15, 8, 8 );
var eyeGeometry2 = new THREE.SphereGeometry( 15, 8, 8 );
var leftEyeball = new THREE.Mesh(eyeGeometry, materialShader);
faceMeshGroup.add(leftEyeball);
var rightEyeball = new THREE.Mesh(eyeGeometry2, materialShader);
faceMeshGroup.add(rightEyeball);
scene.add(faceMeshGroup);

// Initialize 2d canvas
var canvasInput = document.getElementById('canvas');
var cc = canvasInput.getContext('2d');

// Initialize facetracker
var ctracker = new clm.tracker();
ctracker.init(pModel);
ctracker.start(videoInput);

function mainLoop() {
  stats.begin();

  if ( video.readyState === video.HAVE_ENOUGH_DATA ) {
    videoImageContext.drawImage( video, 0, 0, videoImage.width, videoImage.height );
    if ( videoTexture ) {
      videoTexture.needsUpdate = true;
    }
  }

  //var trackedPoints = ctracker.track(videoInput);
  var trackedPoints = ctracker.getCurrentPosition();
  if (trackedPoints == false || faceMesh == null) {
    settings.trackingFailed = true;
  }
  else {
    settings.trackingFailed = false;
    var currentParameters = ctracker.getCurrentParameters();
    var halfPI = Math.PI/2;
    var rotation = halfPI - Math.atan((currentParameters[0]+1)/currentParameters[1]);
    if (rotation > halfPI) {
      rotation -= Math.PI;
    }
    //settings.rotZ = -rotation;

    /*var ptA = originalMesh.geometry.vertices[10].clone();
    var ptB = originalMesh.geometry.vertices[89].clone();

    var meshDist = ptA.sub(ptB);
    var meshScale = meshDist.length();

    var ptA = new THREE.Vector2(trackedPoints[0][0], trackedPoints[0][1]);
    var ptB = new THREE.Vector2(trackedPoints[14][0], trackedPoints[14][1]);

    var trackedDist = ptA.sub(ptB);
    var trackedScale = trackedDist.length();
    settings.scale = trackedScale / meshScale;*/

    faceMesh.geometry.dynamic = true;
    deformer.update(faceMesh, trackedPoints);
    faceMesh.geometry.verticesNeedUpdate = true;

    faceMeshGroup.visible = settings.drawMesh;

    for (var i = 0; i < matchedPoints.length; i++) {
      var pt = faceMesh.geometry.vertices[matching[i][1]].clone();
      pt.applyMatrix4(faceMesh.matrixWorld);
      matchedPoints[i].position.set(pt.x, pt.y, pt.z);
      matchedPoints[i].visible = settings.drawMatchedPoints;
    }

    if (settings.drawWireframe) {
      faceMesh.material = materialWireframe;
      leftEyeball.material = materialWireframe;
      rightEyeball.material = materialWireframe;
    }
    else {
      faceMesh.material = materialShader;
      leftEyeball.material = materialShader;
      rightEyeball.material = materialShader;
      materialShader.uniforms.zOffset.value = settings.zOffset;
      var rot = new THREE.Euler( settings.rotX, settings.rotY, settings.rotZ, 'XYZ' );
      materialShader.uniforms.rotationMatrix.value = new THREE.Matrix4().makeRotationFromEuler(rot);

    }

    // Update Eyeballs
    var sumLeft = new THREE.Vector3(0, 0, 0);
    var sumRight = new THREE.Vector3(0, 0, 0);
    for (var i = 0; i < matching.length; i++) {
      if (leftEyeIndices.indexOf(matching[i][0]) != -1) {
        var pt = faceMesh.geometry.vertices[matching[i][1]].clone();
        sumLeft.add(pt);
      }
      else if (rightEyeIndices.indexOf(matching[i][0]) != -1) {
        var pt = faceMesh.geometry.vertices[matching[i][1]].clone();
        sumRight.add(pt);
      }
    }
    sumLeft.multiplyScalar(1.0 / leftEyeIndices.length);
    leftEyeball.position.set(sumLeft.x, sumLeft.y, sumLeft.z - 10);
    sumRight.multiplyScalar(1.0 / rightEyeIndices.length);
    rightEyeball.position.set(sumRight.x, sumRight.y, sumRight.z - 10);
  }

  renderer.render(scene, camera);

  cc.clearRect(0, 0, canvasInput.width, canvasInput.height);
  if (settings.drawTrackedPoints) {
    ctracker.draw(canvasInput);
  }
  stats.end();
  requestAnimationFrame(mainLoop);
}
mainLoop();
