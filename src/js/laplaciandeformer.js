function LaplacianDeformer() {
  this.weight = 1.0;
  this.neighbors = null;
  this.laplacianCoordinates = null;
  this.matching = null;
  this.A = null;
  this.invA = null;
  this.originalMesh = null;
}

LaplacianDeformer.prototype.initialize = function(mesh, matching) {
  this.originalMesh = mesh.clone();
  this.matching = matching;
  this.calculateNeighbors(mesh);

  this.laplacianCoordinates = [];
  this.calculateLaplacianCoordinates(mesh);

  // Create Laplacian matrix
  this.A = [];
  var noVertices = mesh.geometry.vertices.length;
  var noRows = noVertices*2 + matching.length*2;
  var noCols = noVertices*2;
  for (var i = 0; i < noRows; i++) {
    this.A[i] = [];
    for (var j = 0; j < noCols; j++) {
      this.A[i][j] = 0.0;
    }
  }

  var rowIndex = 0;
  // Build x-coordinates
  for (var i = 0; i < noVertices; i++) {
    this.A[rowIndex][i] = 1.0;
    var valence = this.neighbors[i].length;
    for (var j = 0; j < valence; j++) {
      var neighborIdx = this.neighbors[i][j];
      this.A[rowIndex][neighborIdx] = -1.0 / valence;
    }
    rowIndex++;
  }

  // Build y-coordinates
  for (var i = 0; i < noVertices; i++) {
    this.A[rowIndex][noVertices+i] = 1.0;
    var valence = this.neighbors[i].length;
    for (var j = 0; j < valence; j++) {
      var neighborIdx = this.neighbors[i][j];
      this.A[rowIndex][noVertices+neighborIdx] = -1.0 / valence;
    }
    rowIndex++;
  }

  // Build z-coordinates
  /*for (var i = 0; i < noVertices; i++) {
    this.A[rowIndex][2*noVertices+i] = 1.0;
    var valence = this.neighbors[i].length;
    for (var j = 0; j < valence; j++) {
      var neighborIdx = this.neighbors[i][j];
      this.A[rowIndex][2*noVertices+neighborIdx] = -1.0 / valence;
    }
    rowIndex++;
  }*/

  // Add matching constraints
  for (var i = 0; i < matching.length; i++) {
    var idx = matching[i][1];
    // constrain x
    this.A[rowIndex][idx] = this.weight;
    rowIndex++

    // constrain y
    this.A[rowIndex][noVertices + idx] = this.weight;
    rowIndex++;
  }

  // Constrain one point in z direction;
  //this.A[rowIndex][2*noVertices + matching[0][1]] = 1.0;

  // Decompose matrix
  //var dec = numeric.LU(this.A);

  var pinv = function(A) {
    return numeric.dot(numeric.inv(numeric.dot(numeric.transpose(A),A)),numeric.transpose(A));
  };
  this.invA = pinv(this.A);
};

LaplacianDeformer.prototype.update = function(mesh, constraints) {
  this.calculateLaplacianCoordinates(this.originalMesh);

  var b = [];
  var noCoords = this.laplacianCoordinates.length;
  for (var i = 0; i < noCoords; i++) {
    b[i] = this.laplacianCoordinates[i].x;
    b[noCoords + i] = this.laplacianCoordinates[i].y;
    //b[2*noCoords + i] = this.laplacianCoordinates[i].z;
  }

  for (var i = 0; i < matching.length; i++) {
    var constraintIdx = matching[i][0];
    b[2*noCoords + 2*i] = this.weight * constraints[constraintIdx][0];
    b[2*noCoords + 2*i + 1] = this.weight * 480.0-constraints[constraintIdx][1];
  }
  //b[b.length] = 0.0;

  var x = numeric.dot(this.invA, b);

  var noVertices = mesh.geometry.vertices.length;
  for (var i = 0; i < noVertices; i++) {
    var vertex = mesh.geometry.vertices[i];
    vertex.x = x[i];
    vertex.y = x[noVertices + i];
    //vertex.z = x[2*noVertices + i];
  }
};

LaplacianDeformer.prototype.calculateLaplacianCoordinates = function(mesh) {
  for (var i = 0; i < mesh.geometry.vertices.length; i++) {
    var sum = new THREE.Vector3(0, 0, 0);
    var valence = this.neighbors[i].length;
    for (var j = 0; j < valence; j++) {
      var neighborIdx = this.neighbors[i][j];
      var neighbor = mesh.geometry.vertices[neighborIdx].clone();
      sum.add(neighbor);
    }
    var currentVertex = mesh.geometry.vertices[i].clone();
    sum.multiplyScalar(1.0/valence);
    currentVertex.sub(sum);
    this.laplacianCoordinates[i] = currentVertex;
  }
};

LaplacianDeformer.prototype.calculateNeighbors = function(mesh) {
  this.neighbors = [];
  for (var i = 0; i < mesh.geometry.faces.length; i++) {
    var face = mesh.geometry.faces[i];
    var a = face.a;
    var b = face.b;
    var c = face.c;

    this.addNeighbor(a, b);
    this.addNeighbor(a, c);
    this.addNeighbor(b, a);
    this.addNeighbor(b, c);
    this.addNeighbor(c, a);
    this.addNeighbor(c, b);
  }
};

LaplacianDeformer.prototype.addNeighbor = function(a, b) {
  if (this.neighbors[a] == null) {
    this.neighbors[a] = [];
  }
  if (this.neighbors[a].indexOf(b) == -1) {
    this.neighbors[a].push(b);
  }
};
