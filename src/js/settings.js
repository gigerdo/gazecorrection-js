
// Initialize GUI
var GuiValues = function() {
  this.rotX = 0.0;
  this.rotY = 0.0;
  this.rotZ = 0.0;
  this.drawMesh = true;
  this.drawWireframe = false;
  this.drawTrackedPoints = true;
  this.trackingFailed = true;
  this.zOffset = 0.0;
  this.drawMatchedPoints = false;
};
var settings = new GuiValues();
var gui = new dat.GUI({ autoPlace: false });
var guiContainer = document.getElementById('gui-container');
guiContainer.appendChild(gui.domElement);
var f1 = gui.addFolder('Face Rotation');
f1.add(settings, 'rotX', -1.0, 1.0).listen();
f1.add(settings, 'rotY', -1.0, 1.0).listen();
f1.add(settings, 'rotZ', -1.0, 1.0).listen();
f1.add(settings, 'zOffset', -100, 100);
var f2 = gui.addFolder('Debugging');
f2.add(settings, 'drawMesh');
f2.add(settings, 'drawWireframe');
f2.add(settings, 'drawTrackedPoints');
f2.add(settings, 'drawMatchedPoints');
gui.add(settings, 'trackingFailed').listen();

// Show FPS
var stats = new Stats();
stats.setMode(0);
stats.domElement.style.position = 'absolute';
stats.domElement.style.left = '0px';
stats.domElement.style.top = '0px';
document.getElementById("container").appendChild( stats.domElement );
